-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.31-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for library
CREATE DATABASE IF NOT EXISTS `library` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `library`;

-- Dumping structure for table library.author
CREATE TABLE IF NOT EXISTS `author` (
  `author_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  PRIMARY KEY (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Dumping data for table library.author: ~17 rows (approximately)
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` (`author_id`, `name`, `surname`) VALUES
	(1, 'Ivan', 'Tokin '),
	(2, 'Franc', 'Kafka'),
	(3, 'J.R.R.', 'Tolkin'),
	(4, 'Dejan', 'Tiago Stanković'),
	(5, 'Gabrijel', 'Garsija Markes'),
	(6, 'Branko', 'Ćopić '),
	(7, 'Teri ', 'Pračet'),
	(8, 'Čarls', 'Dikens '),
	(9, 'Svetislav ', 'Basara'),
	(10, 'Dž. D', 'Selindžer'),
	(11, 'Luj', 'de Bernier'),
	(12, 'Dobrica', 'Ćosić'),
	(13, 'Miroslav ', 'Antić'),
	(14, 'Onore', 'de Balzak'),
	(15, 'Albert', 'Kami'),
	(16, 'Lav', 'Tolstoj'),
	(18, 'Fjodor', 'Mihailovič Dostojevski');
/*!40000 ALTER TABLE `author` ENABLE KEYS */;

-- Dumping structure for table library.book
CREATE TABLE IF NOT EXISTS `book` (
  `book_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `original_title` varchar(255) NOT NULL,
  `print_year` year(4) NOT NULL,
  `publication_number` int(10) unsigned NOT NULL,
  `language` varchar(255) NOT NULL,
  `publisher_id` int(10) unsigned NOT NULL,
  `location_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`book_id`),
  UNIQUE KEY `uq_book_publication_number` (`publication_number`),
  KEY `fk_book_location_id` (`location_id`),
  KEY `fk_book_publisher_id` (`publisher_id`),
  CONSTRAINT `fk_book_location_id` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_book_publisher_id` FOREIGN KEY (`publisher_id`) REFERENCES `publisher` (`publisher_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;

-- Dumping data for table library.book: ~17 rows (approximately)
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` (`book_id`, `title`, `original_title`, `print_year`, `publication_number`, `language`, `publisher_id`, `location_id`) VALUES
	(1, 'Boja Magije ', 'The Color of magic', '1998', 123, 'srpski', 2, 3),
	(2, 'Gospodar prstenova Družina prstena', 'The lord of the rings The fellowship oh the ring', '1996', 140, 'srpski', 5, 1),
	(3, 'Gospodar prstenova Dve kule', 'The lord of the rings The two towers', '1996', 125, 'srpski', 5, 1),
	(4, 'Gospodar prstenova Povratak kralja', 'The lord of the rings The return of the king', '1996', 126, 'srpski', 5, 1),
	(6, 'Fama o biciklistima', '', '2007', 127, 'srpski', 1, 2),
	(7, 'Velika očekivanja', 'The great expectations', '2016', 128, 'srpski', 3, 4),
	(8, 'Lovac u žitu', 'The Catcher in the Rye', '2003', 129, 'srpski', 8, 2),
	(9, 'Proces', 'Der Prozess', '2010', 130, 'srpski', 7, 5),
	(10, 'Sto godina samoće', 'Cien anos de soledad', '2002', 131, 'srpski', 6, 3),
	(11, 'Estoril ', '', '2017', 132, 'srpski', 4, 3),
	(12, 'Orlovi rano lete', '', '2003', 133, 'srpski', 10, 4),
	(13, 'Najnormalniji čovek na svetu', '', '2015', 134, 'srpski', 9, 4),
	(14, 'Koreni', '', '2005', 142, 'srpski', 1, 3),
	(15, 'Mandolina Kapetana Korelija', 'Captain Corellis Mandolin', '2001', 143, 'srpski', 11, 2),
	(16, 'Pas', '', '2017', 144, 'srpski', 2, 4),
	(111, 'Rat i mir', '', '2001', 43, 'srpski', 8, 3),
	(112, 'Kockar', '', '2005', 31, 'srpski', 7, 2),
	(113, 'Poštašavili', 'Going Postal', '2005', 1, 'srpski', 2, 2);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;

-- Dumping structure for table library.book_author
CREATE TABLE IF NOT EXISTS `book_author` (
  `b_a_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` int(10) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`b_a_id`),
  KEY `fk_book_author_book_id` (`book_id`),
  KEY `fk_book_author_author_id` (`author_id`),
  CONSTRAINT `fk_book_author_author_id` FOREIGN KEY (`author_id`) REFERENCES `author` (`author_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_book_author_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8;

-- Dumping data for table library.book_author: ~18 rows (approximately)
/*!40000 ALTER TABLE `book_author` DISABLE KEYS */;
INSERT INTO `book_author` (`b_a_id`, `book_id`, `author_id`) VALUES
	(139, 4, 3),
	(140, 6, 9),
	(141, 7, 8),
	(142, 8, 10),
	(143, 9, 2),
	(144, 10, 5),
	(145, 11, 4),
	(146, 12, 6),
	(147, 13, 1),
	(148, 14, 12),
	(149, 15, 11),
	(150, 16, 1),
	(151, 111, 16),
	(152, 112, 18),
	(153, 113, 7),
	(157, 3, 3),
	(158, 2, 3),
	(167, 1, 7);
/*!40000 ALTER TABLE `book_author` ENABLE KEYS */;

-- Dumping structure for table library.book_category
CREATE TABLE IF NOT EXISTS `book_category` (
  `b_c_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`b_c_id`),
  KEY `fk_book_category_book_id` (`book_id`),
  KEY `fk_book_category_category_id` (`category_id`),
  CONSTRAINT `fk_book_category_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_book_category_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;

-- Dumping data for table library.book_category: ~32 rows (approximately)
/*!40000 ALTER TABLE `book_category` DISABLE KEYS */;
INSERT INTO `book_category` (`b_c_id`, `book_id`, `category_id`) VALUES
	(175, 4, 2),
	(176, 6, 7),
	(177, 6, 6),
	(178, 7, 8),
	(179, 7, 7),
	(180, 8, 8),
	(181, 8, 7),
	(182, 9, 9),
	(183, 9, 8),
	(184, 9, 7),
	(185, 10, 8),
	(186, 10, 7),
	(187, 11, 7),
	(188, 11, 6),
	(189, 12, 5),
	(190, 12, 7),
	(191, 13, 7),
	(192, 13, 6),
	(193, 14, 7),
	(194, 15, 7),
	(195, 16, 7),
	(196, 16, 6),
	(197, 111, 8),
	(198, 111, 7),
	(199, 112, 8),
	(200, 112, 7),
	(201, 113, 3),
	(202, 113, 7),
	(205, 3, 2),
	(206, 2, 2),
	(207, 2, 7),
	(218, 1, 7);
/*!40000 ALTER TABLE `book_category` ENABLE KEYS */;

-- Dumping structure for table library.category
CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `uq_category_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table library.category: ~10 rows (approximately)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`category_id`, `name`) VALUES
	(4, 'Biografija'),
	(5, 'Dečiji roman'),
	(2, 'Epska fantastika'),
	(3, 'Fikcija'),
	(9, 'Filozofija'),
	(1, 'Istorijska fantastika'),
	(8, 'Klasik'),
	(10, 'Poezija'),
	(7, 'Roman'),
	(6, 'savremena književnost');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Dumping structure for table library.location
CREATE TABLE IF NOT EXISTS `location` (
  `location_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room` varchar(50) NOT NULL,
  `shelf_number` int(10) NOT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table library.location: ~5 rows (approximately)
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` (`location_id`, `room`, `shelf_number`) VALUES
	(1, 'Dnevna soba', 1),
	(2, 'Dnevna soba', 2),
	(3, 'Radna soba', 1),
	(4, 'Radna soba', 2),
	(5, 'Radna soba', 3);
/*!40000 ALTER TABLE `location` ENABLE KEYS */;

-- Dumping structure for table library.login
CREATE TABLE IF NOT EXISTS `login` (
  `login_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`login_id`),
  KEY `fk_login_user_id` (`user_id`),
  CONSTRAINT `fk_login_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table library.login: ~0 rows (approximately)
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
/*!40000 ALTER TABLE `login` ENABLE KEYS */;

-- Dumping structure for table library.photo
CREATE TABLE IF NOT EXISTS `photo` (
  `photo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `book_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`photo_id`),
  KEY `fk_photo_book_id` (`book_id`),
  CONSTRAINT `fk_photo_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- Dumping data for table library.photo: ~18 rows (approximately)
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;
INSERT INTO `photo` (`photo_id`, `path`, `book_id`) VALUES
	(29, 'img/2.png', 2),
	(30, 'img/3.png', 3),
	(31, 'img/4.png', 4),
	(32, 'img/6.png', 6),
	(33, 'img/7.png', 7),
	(34, 'img/8.png', 8),
	(35, 'img/9.png', 9),
	(36, 'img/10.png', 10),
	(37, 'img/11.png', 11),
	(38, 'img/12.png', 12),
	(39, 'img/13.png', 13),
	(40, 'img/14.png', 14),
	(41, 'img/15.png', 15),
	(42, 'img/16.png', 16),
	(43, 'img/111.png', 111),
	(44, 'img/112.png', 112),
	(45, 'img/113.png', 113),
	(49, 'img/1.png', 1);
/*!40000 ALTER TABLE `photo` ENABLE KEYS */;

-- Dumping structure for table library.publisher
CREATE TABLE IF NOT EXISTS `publisher` (
  `publisher_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `founded_at` year(4) DEFAULT NULL,
  PRIMARY KEY (`publisher_id`),
  UNIQUE KEY `uq_publisher_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table library.publisher: ~10 rows (approximately)
/*!40000 ALTER TABLE `publisher` DISABLE KEYS */;
INSERT INTO `publisher` (`publisher_id`, `name`, `city`, `country`, `founded_at`) VALUES
	(1, 'Dereta', 'Beograd', 'Srbija', '1980'),
	(2, 'Laguna', 'Beograd', 'Srbija', '1998'),
	(3, 'Vulkan', 'Beograd', 'Srbija', '2010'),
	(4, 'Kontrast', 'Beograd', 'Srbija', '2015'),
	(5, 'Esotheria', 'Beograd', 'Srbija', '1990'),
	(6, 'Zavod za udžbenike', 'Beograd', 'Srbija', '1957'),
	(7, 'Prosveta', 'Beograd', 'Srbija', '1901'),
	(8, 'Lom', 'Beograd', 'Srbija', '1989'),
	(9, 'Samizdat', 'Beograd', 'Srbija', '1993'),
	(10, 'Kreativni centar', 'Beograd', 'Srbija', '1980'),
	(11, 'Plato', 'Beograd', 'Srbija', '1989');
/*!40000 ALTER TABLE `publisher` ENABLE KEYS */;

-- Dumping structure for table library.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(128) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `uq_user_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table library.user: ~2 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `username`, `password`) VALUES
	(1, 'tijanaivanovic', '$2y$10$yIqWw4E7rHMuxP5Afi.LCOGOC7Lvrv3lVI4qYFl.hAsGz/AtRDwAq'),
	(3, 'nevenaruzic', '$2y$10$ynbOTN/FQa/aqXowLTwI4.80fF80G87qZ18xm8w1QqfGmmO8YyBpC');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
