<?php
    namespace App\Controllers;
    use App\Core\Role\UserRoleController;

    class UserCategoryManagementController extends UserRoleController {
        public function categories() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();

            $this->set('categories', $categories);            
           
        }

        public function category($id) {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($id);

            if(!$category) {
                header('Location: /vebPraktikum');
                exit;
            }
            $this->set('category', $category);  

            $bookModel = new \App\Models\BookModel($this->getDatabaseConnection());
            $booksInCategory = $bookModel->getByCategory($id);
            if(!$booksInCategory) {
                header('Još uvek nema knjiga u ovoj kategoriji');                
            }

            $this->set('booksInCategory', $booksInCategory);         
            

        }

        public function getEdit($categoryid){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($categoryid);

            if(!$category){
                $this->redirect(\Configuration::BASE . 'user/categories');
            }

            $this->set('category', $category);
            return $categoryModel;

        }

        public function postEdit($categoryid){
            $categoryModel = $this->getEdit($categoryid);

            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);           
            
            $categoryModel->editById($categoryid, [
                'name' => $name,
                               
            ]);

            $this->redirect(\Configuration::BASE . 'user/categories');
        }

        public function getAdd(){

        }

        public function postAdd(){
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);                     

            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categoryid = $categoryModel->add([
                'name' => $name,                
                
            ]);

            if($category==0){    
                return $this->set('message', 'Došlo je do greške! Nije moguće dodati ovu kategoriju');;
                
            }
        }
    }