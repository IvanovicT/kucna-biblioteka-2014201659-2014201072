<?php
    namespace App\Controllers;
    use App\Core\Role\UserRoleController;

    class UserBookManagementController extends UserRoleController {
        public function books() {
            $bookModel = new \App\Models\BookModel($this->getDatabaseConnection());
            $photoModel = new \App\Models\PhotoModel($this->getDatabaseConnection());
            $books = $bookModel->getAllBooks();            
            foreach($books as &$book){
                $book->photo = $photoModel->getByBookId($book->book_id);    
            }            
            
            $this->set('books', $books);           
           
        }

        public function book($id) {
            $bookModel = new \App\Models\BookModel($this->getDatabaseConnection());
            $photoModel = new \App\Models\PhotoModel($this->getDatabaseConnection());
            $authorModel = new \App\Models\AuthorModel($this->getDatabaseConnection());
            $book = $bookModel->getById($id);
            $photo = $photoModel->getByBookId($book->book_id);  
            $bookAuthor = $bookModel->getBookAuthor($book->book_id);           
            $author = $authorModel->getByAuthorId($bookAuthor["author_id"]);                                                  

            $locationModel = new \App\Models\LocationModel($this->getDatabaseConnection());
            $bookLocation = $bookModel->getBookLocation($id);
            $location = $locationModel->getByLocationId($book->location_id);
            if(!$book) {
                header('Location: /vebPraktikum');
                exit;
            }

            $this->set('book', $book);              
            $this->set('photo', $photo); 
            $this->set('author', $author);
            $this->set('booklocation', $bookLocation);
            $this->set('location', $location);            
                                      
        }

        public function getEdit($bookid){
            $bookModel = new \App\Models\BookModel($this->getDatabaseConnection());
            $book = $bookModel->getById($bookid);
            if(!$book){
                $this->redirect(\Configuration::BASE . 'user/books');
            }
            $this->set('book', $book);

            $authorModel = new \App\Models\AuthorModel($this->getDatabaseConnection());
            $bookAuthorModel = new \App\Models\BookAuthorModel($this->getDatabaseConnection());
            $bookAuthors = $bookAuthorModel->getAllByFieldName('book_id',$bookid);

            $empty = array_filter($bookAuthors);
            if(!empty($empty)){
                $authors = [];
                foreach ($bookAuthors as $bookAuthor){
                    $bookAuthor = $authorModel->getById($bookAuthor->author_id);
                    $bookAuthor->name = $bookAuthor->name;
                    $authors[] = $bookAuthor;
                }

                $this->set('bookAuthors', $bookAuthors);
            }

            
            $authors = $authorModel->getAll();
            for ($i=0; $i<count($authors); $i++){
                $authors[$i]->bookAuthors = $bookAuthorModel->getAllByFieldName('author_id', $authors[$i]->author_id);
                
            }
            $this->set('authors', $authors);
            
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $bookCategoryModel = new \App\Models\BookCategoryModel($this->getDatabaseConnection());
            $bookCategories = $bookCategoryModel->getAllByFieldName('book_id', $bookid);

            $empty = array_filter($bookCategories);
            if(!empty($empty)){
                $categories = [];
                foreach ($bookCategories as $bookCategory){
                    $bookCategory = $categoryModel->getById($bookCategory->category_id);
                    $bookCategory->name = $bookCategory->name;
                    $categories[] = $bookCategory;
                }

                $this->set('bookCategories', $bookCategories);
            }

            
            $categories = $categoryModel->getAll();
            for ($i=0; $i<count($categories); $i++){
                $categories[$i]->bookCategories = $bookCategoryModel->getAllByFieldName('category_id', $categories[$i]->category_id);
                
            }
            $this->set('categories', $categories);

            return $bookModel;

        }

        public function postEdit($bookid){
           
            $bookModel = $this->getEdit($bookid);

            $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $original_title = filter_input(INPUT_POST, 'original_title', FILTER_SANITIZE_STRING);
            $print_year = filter_input(INPUT_POST, 'print_year', FILTER_SANITIZE_STRING);
            $publication_number = filter_input(INPUT_POST, 'publication_number', FILTER_VALIDATE_INT);
            $language = filter_input(INPUT_POST, 'language', FILTER_SANITIZE_STRING);
            $publisher_id = filter_input(INPUT_POST, 'publisher_id', FILTER_VALIDATE_INT);
            $location_id = filter_input(INPUT_POST, 'location_id', FILTER_VALIDATE_INT);

            $bookModel->editById($bookid, [
                'title' => $title,
                'original_title' => $original_title,
                'print_year' => $print_year,
                'publication_number' => $publication_number,
                'language' =>  $language,
                'publisher_id' => $publisher_id,
                'location_id' =>$location_id

            ]);

            if(!$bookModel){
                $this->set('mesage', 'Došlo je do greške, nije moguće izmeniti ovu knjigu!');
                return;
            }

            $bookAuthorIds = filter_input(INPUT_POST, 'authors', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

            $authorModel = new \App\Models\AuthorModel($this->getDatabaseConnection());
            $bookAuthorModel = new \App\Models\BookAuthorModel($this->getDatabaseConnection());
            $bookAuthors = $bookAuthorModel->deleteAllBookAuthorsByBookId($bookid);

            foreach ($bookAuthorIds as $bookAuthorId) {
                $bookAuthorModel->add([
                    'book_id' => $bookid,
                    'author_id' => $bookAuthorId
                ]);
            }

            $bookCategoryIds = filter_input(INPUT_POST, 'categories', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $bookCategoryModel = new \App\Models\BookCategoryModel($this->getDatabaseConnection());
            $bookCategories = $bookCategoryModel->deleteAllBookCategoriesByBookId($bookid);

            foreach ($bookCategoryIds as $bookCategoryId) {
                $bookCategoryModel->add([
                    'book_id' => $bookid,
                    'category_id' => $bookCategoryId
                ]);
            }           
           
            $uploadStatus = $this->doImageUpload('photo', $bookid );
            if(!$uploadStatus){
                return;

            }       
          

            $path = strval('img/'.$bookid . '.png');
            
            $photoModel = new \App\Models\PhotoModel($this->getDatabaseConnection());
            $photo = $photoModel->deleteByBookId($bookid);
            $photo = $photoModel->add([
                'path' => $path,
                'book_id' => $bookid

            ]);            

            $this->redirect(\Configuration::BASE . 'user/books');
        }

        public function getAdd(){
            $authorModel = new \App\Models\AuthorModel($this->getDatabaseConnection());
            $authors = $authorModel->getAll();
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();

            $this->set('categories', $categories);
            $this->set('authors', $authors);

        }

        public function postAdd(){
            $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $original_title = filter_input(INPUT_POST, 'original_title', FILTER_SANITIZE_STRING);
            $print_year = filter_input(INPUT_POST, 'print_year', FILTER_SANITIZE_STRING);
            $publication_number = filter_input(INPUT_POST, 'publication_number', FILTER_VALIDATE_INT);
            $language = filter_input(INPUT_POST, 'language', FILTER_SANITIZE_STRING);
            $publisher_id = filter_input(INPUT_POST, 'publisher_id', FILTER_SANITIZE_NUMBER_INT);
            $location_id = filter_input(INPUT_POST, 'location_id', FILTER_SANITIZE_NUMBER_INT);            
           
            $bookModel = new \App\Models\BookModel($this->getDatabaseConnection());
            $bookid = $bookModel->add([
                'title' => $title,
                'original_title' => $original_title,
                'print_year' => $print_year,
                'publication_number' => $publication_number,
                'language' =>  $language,
                'publisher_id' => $publisher_id,
                'location_id' =>$location_id
            ]);                              
            
            if($bookid==0){                  
                          
               return $this->set('message', 'Došlo je do greške ne možete dodati ovu knjigu!');  
                
            }  

            $authorids = filter_input(INPUT_POST, 'authors', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);
            $bookAuthorModel = new \App\Models\BookAuthorModel($this->getDatabaseConnection());                           
                foreach ($authorids as $author_id) {                
                    $bookAuthorModel->add ([
                        'book_id' => $bookid,
                        'author_id' => $author_id
                    ]);
                }                     

            $categoryids = filter_input(INPUT_POST, 'categories', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);
            $bookCategoryModel = new \App\Models\BookCategoryModel($this->getDatabaseConnection());
            foreach ($categoryids as $category_id) {  
                          
                $bookCategoryModel->add ([
                    'book_id' => $bookid,
                    'category_id' => $category_id
                    
                ]);

            }
            $uploadStatus = $this->doImageUpload('photo', $bookid);
            if(!$uploadStatus){
                return;

            }
            $path = strval('img/'.$bookid . '.png');
            
            $photoModel = new \App\Models\PhotoModel($this->getDatabaseConnection());
            $photo = $photoModel->add([
                'path' => $path ,
                'book_id' => $bookid

            ]);

             $this->redirect(\Configuration::BASE . 'user/books');

             
        }        

        private function doImageUpload(string $fieldName, string $fileName): bool{
            unlink(\Configuration::UPLOAD_DIR .'/'. $fileName . '.png');
            $uploadPath = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
            $file = new \Upload\File($fieldName, $uploadPath);
            $file->setName($fileName);
            $file->addValidations([
                new \Upload\Validation\Mimetype("image/png"),
                new \Upload\Validation\Size("3M"),
                                
            ]);
                        
            try {
                $file->upload();
                return true;

            } catch(\Exception $e){
                $this->set('message', 'Greška: ' . implode(', ', $file->getErrors()));
                return false;

            }
        }
    }