<?php
    namespace App\Controllers;

    class PublisherController extends \App\Core\Controller {
        public function show($id) {
            $publisherModel = new \App\Models\PublisherModel($this->getDatabaseConnection());
            $publisher = $publisherModel->getById($id);

            if(!$publisher) {
                header('Location: /vebPraktikum');
                exit;
            }
            $this->set('publisher', $publisher);    
            
            $bookModel = new \App\Models\BookModel($this->getDatabaseConnection());
            $booksByPublisher = $bookModel->getByPublisher($id);
            $this->set('booksByPublisher', $booksByPublisher);

        }

        public function showAll() {
            $publisherModel = new \App\Models\PublisherModel($this->getDatabaseConnection());
            $publishers = $publisherModel->getAll();
            $this->set('publishers', $publishers);              

        }

       

    }