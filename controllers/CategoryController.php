<?php
    namespace App\Controllers;

    class CategoryController extends \App\Core\Controller {
        public function show($id) {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($id);

            if(!$category) {
                header('Location: /vebPraktikum');
                exit;
            }
            $this->set('category', $category);  

            $bookModel = new \App\Models\BookModel($this->getDatabaseConnection());
            $booksInCategory = $bookModel->getByCategory($id);
            if(!$booksInCategory) {
                header('Još uvek nema knjiga u ovoj kategoriji');                
            }

            $this->set('booksInCategory', $booksInCategory);         
            

        }
    }

   