<?php
    namespace App\Controllers;

    class AuthorController extends \App\Core\Controller {
        public function show($id) {
            $authorModel = new \App\Models\AuthorModel($this->getDatabaseConnection());
            $author = $authorModel->getById($id);

            if(!$author) {
                header('Location: /vebPraktikum');
                exit;
            }
            $this->set('author', $author);   
            
            $bookModel = new \App\Models\BookModel($this->getDatabaseConnection());
            $booksByAuthor = $bookModel->getByAuthor($id);
            $this->set('booksByAuthor', $booksByAuthor);            

        }

        public function showAll() {
            $authorModel = new \App\Models\AuthorModel($this->getDatabaseConnection());
            $authors = $authorModel->getAll();
            $this->set('authors', $authors);   
           
           
        }
    }