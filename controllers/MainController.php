<?php
    namespace App\Controllers;
    use App\Validators\StringValidator;

    class MainController extends \App\Core\Controller {
        public function home() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories); 
                      
        }
        public function getLogin(){

        }

        public function getLogout() {
            $this->getSession()->delete('user_id');
            $this->getSession()->save();

               $this->redirect(\Configuration::BASE);

        }
        public function postLogin(){
            $username = \filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
            $password = \filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);
           
            $validanPassword = (new \App\Validators\StringValidator())
                ->setMinLength(7)
                ->setMaxLength(60)
                ->isValid($password);

            if ( !$validanPassword) {
                $this->set('message', 'Doslo je do greške: Lozinka nije ispravnog formata.');
                return;
            }

            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());

             $user = $userModel->getByUsername($username);
                    if (!$user) {
                    $this->set('message', 'Doslo je do greške: Ne postoji korisnik sa tim korisničkim imenom.');
                     return;
                }

                if (!password_verify($password, $user->password)) {
                    sleep(1);
                    $this->set('message', 'Doslo je do greške: Lozinka nije ispravna.');
                    return;
                }

                $this->getSession()->put('user_id', $user->user_id);
                $this->getSession()->save();

                $this->redirect(\Configuration::BASE . 'user/homepage');
        }

        

        

        
    }
    