<?php
    namespace App\Controllers;
    use App\Core\Role\UserRoleController;

    class UserAuthorManagementController extends UserRoleController {
        public function authors() {
            $authorModel = new \App\Models\AuthorModel($this->getDatabaseConnection());
            $authors = $authorModel->getAll();

            $this->set('authors', $authors);            
           
        }

        public function author($id) {
            $authorModel = new \App\Models\AuthorModel($this->getDatabaseConnection());
            $author = $authorModel->getById($id);

            if(!$author) {
                header('Location: /vebPraktikum');
                exit;
            }
            $this->set('author', $author);   
            
            $bookModel = new \App\Models\BookModel($this->getDatabaseConnection());
            $booksByAuthor = $bookModel->getByAuthor($id);
            $this->set('booksByAuthor', $booksByAuthor);            

        }

        public function getEdit($authorid){
            $authorModel = new \App\Models\AuthorModel($this->getDatabaseConnection());
            $author = $authorModel->getById($authorid);

            if(!$author){
                $this->redirect(\Configuration::BASE . 'user/authors');
            }

            $this->set('author', $author);
            return $authorModel;

        }

        public function postEdit($authorid){
            $authorModel = $this->getEdit($authorid);

            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $surname = filter_input(INPUT_POST, 'surname', FILTER_SANITIZE_STRING);
            
            $authorModel->editById($authorid, [
                'name' => $name,
                'surname' => $surname,
                
            ]);

            $this->redirect(\Configuration::BASE . 'user/authors');
        }

        public function getAdd(){

        }

        public function postAdd(){
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $surname = filter_input(INPUT_POST, 'surname', FILTER_SANITIZE_STRING);           

            $authorModel = new \App\Models\AuthorModel($this->getDatabaseConnection());
            $authorid = $authorModel->add([
                'name' => $name,
                'surname' => $surname,
                
            ]);

            if($authorid==0){
                return $this->set('message', 'Došlo je do greške! Nije moguće dodati ovog autora');;
            }

            


        }
    }