<?php
    namespace App\Controllers;

    class BookController extends \App\Core\Controller {
        public function show($id) {
            $bookModel = new \App\Models\BookModel($this->getDatabaseConnection());
            $photoModel = new \App\Models\PhotoModel($this->getDatabaseConnection());
            $authorModel = new \App\Models\AuthorModel($this->getDatabaseConnection());
            $book = $bookModel->getById($id);
            $photo = $photoModel->getByBookId($book->book_id);  
            $bookAuthor = $bookModel->getBookAuthor($book->book_id);           
            $author = $authorModel->getByAuthorId($bookAuthor["author_id"]);                                                  

            #$locationModel = new \App\Models\LocationModel($this->getDatabaseConnection());
            $bookLocation = $bookModel->getBookLocation($book->book_id);
            
            $this->set('book', $book);              
            $this->set('photo', $photo); 
            $this->set('author', $author);
            $this->set('booklocation', $bookLocation);
                       
                                      
        }

        public function showAll() {
            $bookModel = new \App\Models\BookModel($this->getDatabaseConnection());
            $photoModel = new \App\Models\PhotoModel($this->getDatabaseConnection());
            $books = $bookModel->getAllBooks();
            
            foreach($books as &$book){
                $book->photo = $photoModel->getByBookId($book->book_id);    
            }            
            
            $this->set('books', $books); 
              
        }     
        
        private function normaliseKeywords(string $keywords): string {
            $keywords = trim($keywords);
            $keywords = preg_replace('/ +/', ' ', $keywords);

            return $keywords;

        }      

        public function search(){
            $bookModel = new \App\Models\BookModel($this->getDatabaseConnection());

            $q = filter_input(INPUT_POST, 'q', FILTER_SANITIZE_STRING);

            $keywords = $this->normaliseKeywords($q);               
               
            $books =  $bookModel->search($q); 

            $this->set('books', $books);           

        }

        public function combinedSearch() {
            $bookModel = new \App\Models\BookModel($this->getDatabaseConnection());

            $q1 = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $q2 = filter_input(INPUT_POST, 'author', FILTER_SANITIZE_STRING);
            $q3 = filter_input(INPUT_POST, 'category', FILTER_SANITIZE_STRING);
            $q4 = filter_input(INPUT_POST, 'publisher', FILTER_SANITIZE_STRING);
            $q5 = filter_input(INPUT_POST, 'print_year', FILTER_SANITIZE_STRING);
                    

            $keyword1 = $this->normaliseKeywords($q1);  
            $keyword2 = $this->normaliseKeywords($q2); 
            $keyword3 = $this->normaliseKeywords($q3); 
            $keyword4 = $this->normaliseKeywords($q4);
            $keyword5 = $this->normaliseKeywords($q5);
                         
               
            $books =  $bookModel->detailedSearch($q1, $q2, $q3, $q4, $q5); 

            $this->set('books', $books);

        }
        
    }