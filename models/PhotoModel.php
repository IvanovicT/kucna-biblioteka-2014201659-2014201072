<?php
    namespace App\Models;
    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;

    class PhotoModel extends Model {
        public function getByPhotoId(int $photoid) {
            $sql = 'SELECT * FROM photo WHERE photo_id = ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$photoId]);
            $photos = NULL;
            if ($res) {
                $photos = $prep->fetch(\PDO::FETCH_OBJ);
            }
            return $photos;
        }

        public function getByBookId(int $bookid) {
            $sql = 'SELECT * FROM photo WHERE book_id = ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$bookid]);
            $photos = NULL;
            if ($res) {
                $photos = $prep->fetch(\PDO::FETCH_OBJ);
            }
            return $photos;
        }
        public function deleteByBookId(int $bookid){
            $sql = 'DELETE FROM photo WHERE book_id = ?';

            $prep = $this->getConnection()->prepare($sql);                
            if(!$prep) {
                return [];
            }
            return $prep->execute([$bookid]);
            return $prep->fetchAll(\PDO::FETCH_OBJ);

        }

     
        protected function getFields(): array {
            return [
                'photo_id' => new Field((new NumberValidator())->setIntegerLength(10), false),
                'path' => new Field((new StringValidator())->setMaxLength(255) ),
                'book_id' => new Field((new NumberValidator())->setIntegerLength(10))

            ];
        }
        
    }