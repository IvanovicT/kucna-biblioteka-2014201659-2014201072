<?php
    namespace App\Models;
    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;

    class PublisherModel extends Model{
        protected function getFields(): array{
            return[
                'publisher_id' => new Field((new NumberValidator())->setIntegerLength(10), false),
                'name' => new Field((new StringValidator())->setMaxLength(255) ),
                'city' => new Field((new StringValidator())->setMaxLength(255) ),
                'country' => new Field((new StringValidator())->setMaxLength(255) ),
                'founded_at' => new Field((new NumberValidator())->setIntegerLength(11),true)


            ];
        }
    
    }