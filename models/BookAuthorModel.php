<?php
    namespace App\Models;
    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;

    class BookAuthorModel extends Model{
        protected function getFields(): array {
            return [
                'book_author_id' => new Field((new NumberValidator())->setIntegerLength(10), false),
                'book_id' => new Field((new NumberValidator())->setIntegerLength(10)),
                'author_id' => new Field((new NumberValidator())->setIntegerLength(10))
            ];

        }

        public function getAllBookAuthorsByBookId(int $bookid):array {
            return $this->getAllByFieldName('book_id', $bookid);
        }

        public function deleteAllBookAuthorsByBookId(int $bookid){
            $sql = 'DELETE FROM book_author WHERE book_id = ?';

            $prep = $this->getConnection()->prepare($sql);                
            if(!$prep) {
                return [];
            }
            return $prep->execute([$bookid]);
            return $prep->fetchAll(\PDO::FETCH_OBJ);

        }
    }