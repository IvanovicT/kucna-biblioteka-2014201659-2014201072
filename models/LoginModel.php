<?php
    namespace App\Models;
    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;

    class LoginModel extends Model {
        protected function getFields() : array {
            return[
                'login_id' => new Field((new NumberValidator())->setIntegerLength(10), false),
                'user_id' =>  new Field((new NumberValidator())->setMaxLength(10), false ),
                'recordet_at' =>  new Field((new DateTimeValidator())->allowDate()->allowTime() , false)

            ];
        }
       
    }