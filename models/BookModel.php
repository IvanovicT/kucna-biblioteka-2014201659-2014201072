<?php
    namespace App\Models;
    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;

    class BookModel extends Model { 
        public function getByCategory(int $categoryid): array {
            $sql = 'SELECT * FROM book INNER JOIN book_category on book.book_id=book_category.book_id WHERE category_id = ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$categoryid]);
            $books = NULL;
            if ($res){
                $books = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
            return $books;

        }

        public function getByAuthor(int $authorid): array {
            $sql = 'SELECT * FROM book INNER JOIN book_author on book.book_id=book_author.book_id WHERE author_id = ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$authorid]);
            $books = NULL;
            if ($res){
                $books = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
            return $books;

        }

        public function getByPublisher(int $publisherid): array {
            $sql = 'SELECT * FROM book WHERE publisher_id = ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$publisherid]);
            $books = NULL;
            if ($res){
                $books = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
            return $books;

        }

        public function getBookAuthor($bookid) {
            $sql = 'SELECT * FROM book_author WHERE book_id = ?';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$bookid]);
            $author = null;
            if ($res) {
                $author = $prep->fetch();
            }
            return $author;
        }

        public function getBookLocation ($bookid) {
            $sql = 'SELECT * FROM book INNER JOIN [location] ON book.location_id= [location].location_id WHERE book_id = ?';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$bookid]);
            $location_id = null;
            if ($res) {
                $location_id = $prep->fetch();
            }
            return $location_id;
        }

        public function getAllBooks(){
            $sql = 'SELECT * FROM book;';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute();
            $books = null;
            if ($res) {
                $books = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
           
            return $books;

        }

        protected function getFields(): array {
            return [
                'book_id' => new Field((new NumberValidator())->setIntegerLength(10), false),
                'title' => new Field((new StringValidator(0, 255)) ),
                'original_title' => new Field((new StringValidator(0, 255)) ),
                'print_year' => new Field((new NumberValidator())->setIntegerLength(4)),
                'publication_number' => new Field((new NumberValidator())->setIntegerLength(10)),
                'language' => new Field((new StringValidator(0, 255)) ),
                'publisher_id' => new Field((new NumberValidator())->setIntegerLength(10)),
                'location_id' => new Field((new NumberValidator())->setIntegerLength(10))
            ];

        }

        public function search(string $keyword){
            $sql = 'SELECT * FROM book WHERE title LIKE ? OR original_title LIKE ? ';                                                                
                                                                                                               
            $keyword = '%' . $keyword . '%';
            
            $prep = $this->getConnection()->prepare($sql);                
            if(!$prep) {
                return [];
            }
            $res = $prep->execute([$keyword,$keyword]);                      
            $books = [];          
            if (!$res) {
                return [];                
            }       
            
                       
            return $books = $prep->fetchAll(\PDO::FETCH_OBJ);
            
        }

        public function detailedSearch(string $keyword1, $keyword2, $keyword3, $keyword4, $keyword5){
            $sql = 'SELECT * FROM book INNER JOIN book_author   ON book.book_id=book_author.book_id 
                                       INNER JOIN author        ON author.author_id=book_author.author_id																	 
                                       INNER JOIN book_category ON book.book_id=book_category.book_id 
                                       INNER JOIN category      ON category.category_id=book_category.category_id																	 
                                       INNER JOIN publisher     ON book.publisher_id=publisher.publisher_id
                    WHERE (book.title LIKE ? OR book.original_title LIKE ?) AND (author.name LIKE ? OR author.surname LIKE ? ) AND category.name LIKE ?  AND publisher.name LIKE ? AND book.print_year LIKE ? ' ;

            $keyword1 = '%' . $keyword1 . '%'; 
            $keyword2 = '%' . $keyword2 . '%';   
            $keyword3 = '%' . $keyword3 . '%';   
            $keyword4 = '%' . $keyword4 . '%'; 
            $keyword5 = '%' . $keyword5 . '%';         

            $prep = $this->getConnection()->prepare($sql);                                                                    
            if(!$prep){
                return [];
            }

            $res = $prep->execute([$keyword1,$keyword1, $keyword2, $keyword2, $keyword3,$keyword4, $keyword5]);
            $books = [];
            if(!$res){
                return  [];
            }

            return $books = $prep->fetchAll(\PDO::FETCH_OBJ);


        }

       
    }