<?php
    namespace App\Models;
    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;

    class BookCategoryModel extends Model{
        protected function getFields(): array {
            return [
                'book_category_id' => new Field((new NumberValidator())->setIntegerLength(10), false),
                'book_id' => new Field((new NumberValidator())->setIntegerLength(10)),
                'category_id' => new Field((new NumberValidator())->setIntegerLength(10))
            ];

        }

        public function getAllBookCategoriesByBookId(int $bookid):array {
            return $this->getAllByFieldName('book_id', $bookid);
        }

        public function deleteAllBookCategoriesByBookId(int $bookid){
            $sql = 'DELETE FROM book_category WHERE book_id = ?';

            $prep = $this->getConnection()->prepare($sql);                
            if(!$prep) {
                return [];
            }
            return $prep->execute([$bookid]);
            return $prep->fetchAll(\PDO::FETCH_OBJ);

        }
    }