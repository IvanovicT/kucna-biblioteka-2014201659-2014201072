<?php
    namespace App\Models;
    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;

    class AuthorModel extends Model{
        public function getByAuthorId(int $authorid){
            $sql = 'SELECT * FROM author WHERE author_id = ?';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$authorid]);
            $author = NULL;
            if ($res){
                $author = $prep->fetch();
            }
            return $author;
        }
        
        protected function getFields(): array {
            return [
                'author_id' => new Field((new NumberValidator())->setIntegerLength(10), false),
                'name' => new Field((new StringValidator(0, 255))),
                'surname' => new Field((new StringValidator(0, 255)) )
            ];

        }

    }
      