<?php
    namespace App\Models;
    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;

    class UserModel extends Model{     

        public function getByUsername(string $username){
            $sql = 'SELECT * FROM user WHERE username = ? ;';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$username]);
            $user = NULL;
            if ($res){
                $user = $prep->fetch(\PDO::FETCH_OBJ);
            }
            return $user;

        }

        protected function getFields() : array {
            return[
                'user_id' => new Field((new NumberValidator())->setIntegerLength(10), false),
                'username' =>  new Field((new StringValidator())->setMaxLength(255) ),
                'password' =>  new Field((new StringValidator())->setMaxLength(128) )

            ];
        }

    }