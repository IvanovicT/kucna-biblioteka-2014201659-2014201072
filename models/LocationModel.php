<?php
    namespace App\Models;
    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;

    class LocationModel extends Model {
        public function getByLocationId(int $locationid){
            $sql = 'SELECT * FROM [location] WHERE location_id = ?';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$locationid]);
            $location = NULL;
            if ($res){
                $location = $prep->fetch();
            }
            return $location;
        }

        protected function getFields() : array {
            return[
                'location_id' => new Field((new NumberValidator())->setIntegerLength(10), false),
                'room' =>  new Field((new StringValidator())->setMaxLength(50) ),
                'shelf_number' =>  new Field((new NumberValidator())->setIntegerLength(10), true)

            ];
        }

    }