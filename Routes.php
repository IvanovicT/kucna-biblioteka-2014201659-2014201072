<?php
    return [  
        App\Core\Route::get('|^login$|', 'Main', 'getLogin'),
        App\Core\Route::post('|^login$|', 'Main', 'postLogin'),

        App\Core\Route::get('|^logout$|', 'Main', 'getLogout'),

        App\Core\Route::post('|^search$|', 'Book', 'search'),
        App\Core\Route::post('|^combinedsearch$|', 'Book', 'combinedSearch'),

        App\Core\Route::get('|^book/([0-9]+)/?$|', 'Book', 'show'),
        App\Core\Route::get('|^books$|', 'Book', 'showAll'),

        App\Core\Route::get('|^category/([0-9]+)/?$|', 'Category', 'show'),
        App\Core\Route::get('|^category/book/([0-9]+)/?$|', 'Book', 'show'),

        App\Core\Route::get('|^authors$|', 'Author', 'showAll'),
        App\Core\Route::get('|^author/([0-9]+)/?$|', 'Author', 'show'),
        App\Core\Route::get('|^author/book/([0-9]+)/?$|', 'Book', 'show'),

        App\Core\Route::get('|^publishers$|', 'Publisher', 'showAll'),
        App\Core\Route::get('|^publisher/([0-9]+)/?$|', 'Publisher', 'show'),
        App\Core\Route::get('|^publisher/book/([0-9]+)/?$|', 'Book', 'show'),

        
        #samo ulogovani user pristupa ovim rutama
        App\Core\Route::get('|^user/homepage$|', 'UserHomePage', 'homepage'),

        #books
        App\Core\Route::get('|^user/books$|', 'UserBookManagement', 'books'),
        App\Core\Route::get('|^user/book/([0-9]+)/?$|', 'UserBookManagement', 'book'),
        App\Core\Route::get('|^user/books/edit/([0-9]+)/?$|', 'UserBookManagement', 'getEdit'),
        App\Core\Route::post('|^user/books/edit/([0-9]+)/?$|', 'UserBookManagement', 'postEdit'),
        App\Core\Route::get('|^user/books/add$|', 'UserBookManagement', 'getAdd'),
        App\Core\Route::post('|^user/books/add$|', 'UserBookManagement', 'postAdd'),
        
        #authors
        App\Core\Route::get('|^user/authors$|', 'UserAuthorManagement', 'authors'),
        App\Core\Route::get('|^user/author/([0-9]+)/?$|', 'UserAuthorManagement', 'author'),
        App\Core\Route::get('|^user/authors/edit/([0-9]+)/?$|', 'UserAuthorManagement', 'getEdit'),
        App\Core\Route::post('|^user/authors/edit/([0-9]+)/?$|', 'UserAuthorManagement', 'postEdit'),
        App\Core\Route::get('|^user/authors/add$|', 'UserAuthorManagement', 'getAdd'),
        App\Core\Route::post('|^user/authors/add$|', 'UserAuthorManagement', 'postAdd'),
        
        #categories
        App\Core\Route::get('|^user/categories$|', 'UserCategoryManagement', 'categories'),
        App\Core\Route::get('|^user/category/([0-9]+)/?$|', 'UserCategoryManagement', 'category'),
        App\Core\Route::get('|^user/categories/edit/([0-9]+)/?$|', 'UserCategoryManagement', 'getEdit'),
        App\Core\Route::post('|^user/categories/edit/([0-9]+)/?$|', 'UserCategoryManagement', 'postEdit'),
        App\Core\Route::get('|^user/categories/add$|', 'UserCategoryManagement', 'getAdd'),
        App\Core\Route::post('|^user/categories/add$|', 'UserCategoryManagement', 'postAdd'),


        App\Core\Route::any('|^.*$|', 'Main', 'home')
        
    ];